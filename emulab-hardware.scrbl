#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "hardware" #:version apt-version]{Hardware}

@section[#:tag "emulab-mothership"]{Emulab Cluster}

The Emulab cluster at the University of Utah contains almost 500 servers across
four CPU generations with a total of more than 3,800 cores.

@; XXX don't use apturl right now
More technical details can be found at
@url["https://www.emulab.net/hardware.php"].

@(nodetype "d430" 160 "Haswell, 16 core, 3 disks"
    (list "CPU" "Two Intel E5-2630v3 8-Core CPUs at 2.4 GHz (Haswell)")
    (list "RAM"  "64GB ECC Memory (8x 8 GB DDR4 2133MT/s)")
    (list "Disk" "One 200 GB 6G SATA SSD")
    (list "Disk" "Two 1 TB 7.2K RPM 6G SATA HDDs")
    (list "NIC"  "Two or four Intel I350 1GbE NICs")
    (list "NIC"  "Two or four Intel X710 10GbE NICs"))

@(nodetype "d820" 16 "Sandy Bridge, 32 core, 7 disks"
    (list "CPU" "Four Intel E5-4620 8-Core CPUs at 2.2 GHz (Sandy Bridge)")
    (list "RAM"  "128GB ECC Memory (8x 16 GB DDR3 1333MHz)")
    (list "Disk" "One 250 GB 7.2K RPM 3G SATA HDD")
    (list "Disk" "Six 600 GB 10K RPM 3G SAS HDDs")
    (list "NIC"  "Four Intel X520 10GbE NICs"))

@(nodetype "d710" 160 "Nehalem, 4 core, 2 disks"
    (list "CPU" "One Intel Xeon E5530 4-Core CPU at 2.4 GHz (Nehalem)")
    (list "RAM"  "12GB ECC Memory (6x 2 GB DDR2 1066MHz)")
    (list "Disk" "One 500 GB 7.2K RPM SATA HDD")
    (list "Disk" "One 250 GB 7.2K RPM SATA HDD")
    (list "NIC"  "Four Broadcom BCM5709 1GbE NICs"))

@(nodetype "pc3000" 160 "Nocona, 1 core, 2 disks"
    (list "CPU" "One Intel Xeon single-core CPU at 3.0 GHz (Nocona)")
    (list "RAM"  "2GB ECC Memory (2x 1 GB DDR2 400MHz)")
    (list "Disk" "Two 146 GB 10K RPM SCSI HDD")
    (list "NIC"  "1-4 Intel 1GbE NICs")
    (list "NIC"  "1-4 Intel 10/100 NICs"))

All nodes are connected to two networks:

@itemlist[
    @item{A 100Mb and 1 Gb @italic{Ethernet} @bold{``control network''}---this network
        is used for remote access, experiment management, etc., and is
        connected to the public Internet. When you log in to nodes in your
        experiment using @code{ssh}, this is the network you are using.
        @italic{You should not use this network as part of the experiments you
        run in Emulab.}
    }

    @item{A 10/100Mb, 1/10Gb Ethernet
        @bold{``experiment network''}--each node has at least four
        interfaces on this network.  This network is implemented using multiple
	Cisco, HP, Arista, and Dell (Force10) switches.
     }
]
