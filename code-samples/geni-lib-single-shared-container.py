"""An example of constructing a profile with a single Docker container.

Instructions: Wait for the profile instance to start, and then log in to
the container via the ssh port specified below.  By default, your
container will run a standard Ubuntu image with the Emulab software
preinstalled.
"""

import geni.portal as portal
import geni.rspec.pg as rspec

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()
 
# Create a Docker container.
node = request.DockerContainer("node")

# Request a container hosted on a shared container host; you will not
# have access to the underlying physical host, and your container will
# not be privileged.  Note that if there are no shared hosts available,
# your experiment will be assigned a physical machine to host your container.
node.exclusive = True

# Print the RSpec to the enclosing page.
portal.context.printRequestRSpec()
